<?php

use PiwikLoader\DataLoader\CSVFileLoader;
use PiwikLoader\SiteLoader;

require_once __DIR__ . '/../vendor/autoload.php';

if (!empty($_POST)) {

    $site = $_POST['piwik_url'];
    $authToken = $_POST['piwik_auth_token'];
    $file = $_FILES['file'];

    if (empty($site) || empty($file)) {

        echo '<p style="font-weight: bolder; color: red;">Not enough parameters!</p>';

    } else {

        $dataLoader = new CSVFileLoader();
        $dataLoader->setFilename($file['tmp_name']);

        $loader = new SiteLoader();
        $loader->setSite($site);
        $loader->setTokenAuth($authToken);
        $loader->setDataLoader($dataLoader);
        $loader->load();

        $result = $loader->getResult();

        foreach ($result as $i => $item) {
            $itemXml = new SimpleXMLElement($item);
            ?>
                <p>
                Item <?php echo $i; ?>
                    <?php if ($itemXml->error): ?>
                        <span style="color: red;">Error: <?php echo $itemXml->error['message']; ?></span>
                    <?php else: ?>
                        <span style="color: green;">Success! New site id: <?php echo $itemXml; ?></span>
                    <?php endif; ?>
                </p>
            <?php
        }
    }

} else {
?>
<html>
<head>
    <title>Piwik Loader Demo App</title>
</head>
<body>
<form action="" method="POST" enctype="multipart/form-data">
    <table>
        <tr>
            <td>Piwik URL</td>
            <td><input type="text" name="piwik_url" /></td>
        </tr>
        <tr>
            <td>Piwik Auth Token</td>
            <td><input type="text" name="piwik_auth_token" /></td>
        </tr>
        <tr>
            <td>CSV File</td>
            <td><input type="file" name="file" /></td>
        </tr>
        <tr>
            <td></td>
            <td><input type="submit" value="Load"></td>
        </tr>
    </table>
</form>
</body>
</html>
<?php
}